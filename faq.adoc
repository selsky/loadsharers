= Loadsharers FAQ =

== What makes you think this can work? ==

Population ratio.  There aren't many LBIPs, probably no more than a
thousand worldwide.  The population of technical people who owe their
well-paid jobs and their nice lives in part to what LBIPs do, 'and
know that', is much larger.

Thus, we should be able to spread the LBIP maintenance load across
enough donors to make it painless for any individual to do their bit.

In the U.S. alone there are around 7 million people with jobs in
the technology sector that are directly dependent on LBIP work.  About
160 loadsharers will cover $5K per month basic maintenance for 1
LBIP, implying that the need for loadsharers should start to top out
at about 160K.  That means we only need less than 3% of U.S. tech
workers to become loadsharers to cover the problem.

== Why fund individuals rather than projects? ==

LBIP projects don't normally have enough legal existence to be
funded as projects.

LBIPs tend to have long service lives and to be worth funding 
whatever they happen to be doing at the moment, so a contribution
system that follows LBIPs around rather than being project-focused
makes functional sense.

There's nothing preventing a project from setting up its own creator
account on Patreon or SubscribeStar or whatever, and nothing stopping
loadsharers from contributing to such project accounts.

== Why have advisers at all? ==

The design includes advisers because if you aren't already deeply
wired into the communities that produce LBIPs, it's hard to tell
who you should support.

Advisors are people who (a) want to help loadsharers with that
discovery problem, and (b) have a trust relationship with the
Loadsharers network so you will have a reason to trust them.

You are perfectly free to be a loadsharer without paying any attention
to our advisers at all - all you need to do is take the pledge and
follow up on it.  But they are offering you the use of their
expertise; if you do so, please reward them with some support.

== How do I becomne an adviser? ==

By persuading one of the existing advisers to link to your page
of advice.  It's a decentralized trust network.

There's a list of advisers on the index page of this site. That
list is official, but not exhaustive.  It represents the
rough conseensus of the advisers about who they are.

We have a page link:advising.html[on being a helpful adviser].  If
you do the things it recommends, you are more likely to be listed.

== What remittance methods can I use with Loadsharers? ==

Anything that can get recurring payments from you to an LBIP is fine:
Patreon, SubscribeStar, Liberapay, Paypal's periodic-payment feature,
whatever.

Loadsharers itself is a web of trust relationships among people.
It doesn't care what the transfer methods are.

== Why not a centralized Loadsharers website or 501(c)3 nonprofit? ==

It's been tried, specifically by the founder of Loadsharers, and failed.

It turns out that recruiting people who are both competent to run
an organization like that and able to sustain the effort is really
hard.

Also, organizations that handle money have high complexity, overhead,
and management costs. Remittance systems offer us a way to route
around most of those costs. Loadsharers is designed to be the thinnest
possible coordination layer over the remittance systems.

Last but not least, centralization creates single points of failure.
A loose network like Loadsharers should be less vulnerable to
individual incompetence, political capture, corruption, etc.

== Why don't you hit up corporations to contribute? ==

That's been tried, too, and has largely failed.

A few infrastructure projects, like the Linux kernel, can draw
corporate support because even a nontechnical manager can grasp
the connections between them and profits.  But most LBIP projects
can't - the connection isn't obvious enough, or the project
is not structured enough to cope with bureaucratic funding
requirements; or for any one of several other reasons.

Ranting about how billion-dollar Big Tech corporations are failing a
group of people that is crucial to their continued functioning may
feel satisfying and be entirely justified, but it's not helpful.
Mostly their incentives are to exploit LBIP labor and ignore the
LBIPs. Until those incentives change, the behavior won't either.

Once Loadsharers becomes visible to them, some corporations may choose
to join Loadsharers as a matter of good PR or marketing.  That's fine,
they can contribute the same way individuals do and use the same
advisers.

== Won't people try to free-ride or scam a system like this? ==

Undoubtedly.  Nobody can guarantee no waste in the system,

However, all our advisers are experienced infrastructure developers
with deep connections in the communities and culture that produces
LBIPs. They're not likely to fail their diligence very often or 
for very long.

If you nevertheless expect the amount of successful fraud to be high
enough to bother you or make Loadsharers useless, we encourage you 
to do your own LBIP discovery instead. Or even form your own adviser
network.  There's no harm to us if you do that.

Every individual in the Loadsharers network has total control over where
their money goes.  That eliminates a lot of failure modes right there.


// end
